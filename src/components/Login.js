import React, { Component } from "react";
import Slider from "./Slider";
import { connect } from 'react-redux';
import form_logo from '../public/images/credin-logo.png';
import user_icon from '../public/images/user-icon.svg';
import pass_icon from '../public/images/password-icon.svg';
import '../components/login-signup.css';
import '../components/cta-btn.css';
import { Link } from "react-router-dom";
import { useFormik } from "formik";

const Login = () => {
    const formik = useFormik({
        initialValues: {
          user_name: '',
          password: '',
        },
        validate:(values) =>{
            const error = {};
            if (!values.user_name) {
                error.user_name = "Please enter user name."
            }
            if (!values.password) {
                error.password = "Please enter password."
            }
            return error;
        },
        onSubmit: values => {
          alert(JSON.stringify(values, null, 2));
        },
      });
    return (
        <div className="account-wrap">
            <div className='row no-gutters'>
                <div className="col-lg-8">
                    <Slider />
                </div>
                <div className="col-lg-4">
                    <div className="form-wrap account-form">
                        <div className="account-form-logo">
                            <img src={form_logo} />
                        </div>
                        <form onSubmit={formik.handleSubmit}>
                            <h3 className="account-form-title">Log In</h3>

                            <div className="form-group">
                                <img src={user_icon} alt="" />
                                <input type="text" value={formik.values.user_name} onChange={(e) => formik.setFieldValue('user_name',e.target.value)} className="form-control" placeholder="USERNAME / EMAIL" />
                            </div>
                            {formik.errors.user_name &&
                                formik.touched.user_name && 
                                <span style={{color:"red"}}>{formik.errors.user_name}</span>}

                            <div className="form-group">
                                <img src={pass_icon} alt="" />
                                <input type="password" value={formik.values.password} onChange={(e) => formik.setFieldValue('password',e.target.value)} className="form-control" placeholder="PASSWORD" />
                            </div>
                            {formik.errors.password &&
                                formik.touched.password && 
                                <span style={{color:"red"}}>{formik.errors.password}</span>}
                            {/* <div className="form-group">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" id="customCheck1" />
                                    <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                                </div>
                            </div> */}
                            <div className="submit-btn">
                                <button type="submit" className="cta-btn">Log In</button>
                            </div>
                            <div className="author-form-note">
                                By creating an account you agree to our <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a>
                               <p>If you don't have an account? <Link to="/sign-up">Create new account</Link></p>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>      
        </div>
    );
}

        // if (!localStorage.getItem('token')) {
        //     this.props.history.push('/sign-up')
        // }
        

export default Login