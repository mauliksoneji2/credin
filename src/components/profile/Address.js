import React from 'react'
import { Button } from 'react-bootstrap'
import { Form } from 'react-bootstrap'
function Address() {
    return (
        <div>
            <Form>
                <Form.Group controlId="formBasicEmail">
                    <Form.Control type="first_name"  placeholder="FIRST NAME" />
                </Form.Group>

                <Form.Group controlId="formBasicEmail">
                    <Form.Control type="last_name"  placeholder="LAST NAME" />
                </Form.Group>
                <Form.Group controlId="formBasicEmail">
                    <Form.Control type="business_name"  placeholder="BUSINESS NAME" />
                </Form.Group>
                <div className="form-submit">
                    <Button className="cta-btn cta-blue" type="submit">Update</Button>
                </div>
            </Form>
        </div>
    )
}

export default Address
