import React, { Component } from "react";
import Slider from "./Slider";
import form_logo from '../public/images/credin-logo.png';
import user_icon from '../public/images/user-icon.svg';
import mail_icon from '../public/images/mail-icon.svg';
import '../components/login-signup.css';
import '../components/cta-btn.css';
import { useFormik } from "formik";
import { Link } from "react-router-dom";

const SignUp = () => {
    const formik = useFormik({
        initialValues: {
          email: '',
          user_name: '',
          mobile: '',
        },
        validate:(values) =>{
            const error = {};
            if (!values.email) {
                error.email = "Please enter email."
            }
            if (values.email) {
                var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                if (!pattern.test(values.email)) {
                  error.email = "Please enter valid email address.";
                }
            }
            if (!values.user_name) {
                error.user_name = "Please enter user name."
            }
            if (!values.mobile) {
                error.mobile = "Please enter mobile."
            }
            return error;
        },
        onSubmit: values => {
          alert(JSON.stringify(values, null, 2));
        },
      });
    return (
        <div className="account-wrap">
            <div className='row no-gutters'>
                <div className="col-lg-8">
                    <Slider />
                </div>
                <div className="col-lg-4">
                    <div className="form-wrap account-form">
                        <div className="account-form-logo">
                            <img src={form_logo} />
                        </div>
                        <form onSubmit={formik.handleSubmit}>
                            <h3 className="account-form-title">Sign Up</h3>

                            <div className="form-group">
                                <img src={mail_icon} alt="" />
                                <input type="email" value={formik.values.email} onChange={(e) => formik.setFieldValue('email',e.target.value)} className="form-control" placeholder="EMAIL" />
                            </div>
                            {formik.errors.email &&
                                formik.touched.email && 
                                <span style={{color:"red"}}>{formik.errors.email}</span>}

                            <div className="form-group">
                                <img src={user_icon} alt="" />
                                <input type="text" value={formik.values.user_name} onChange={(e) => formik.setFieldValue('user_name',e.target.value)} className="form-control" placeholder="USERNAME" />
                            </div>
                            {formik.errors.user_name &&
                                formik.touched.user_name && 
                                <span style={{color:"red"}}>{formik.errors.user_name}</span>}

                            <div className="form-group">
                                <img src={user_icon} alt="" />
                                <input type="number" minLength="10" value={formik.values.mobile} onChange={(e) => formik.setFieldValue('mobile',e.target.value)} className="form-control" placeholder="MOBILE" />
                            </div>
                            {formik.errors.mobile &&
                                formik.touched.mobile && 
                                <span style={{color:"red"}}>{formik.errors.mobile}</span>}
                            <div className="submit-btn">
                                <button type="submit" className="cta-btn">Sign Up</button>
                            </div>
                            <div className="author-form-note">
                                By creating an account you agree to our <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a>
                                <p>If you have already account <Link to="/sign-in">Log In</Link>.</p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>  
    );
}
export default SignUp